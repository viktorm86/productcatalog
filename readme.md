# Starting up application on development environment

## Prerequisites

- .NET Core 2.1
- npm
- MS Sql Server

## Prepare database

Docker can be used to quickly spin up own MS Sql Server instance:

```bash
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Passw0rd!' -e 'MSSQL_PID=Express' -p 1433:1433 -d microsoft/mssql-server-linux:latest
```

- Change connection string in ```./appsettings.Development.json``` to correspond to your sqlserver installation
- Change path to folder with photos in ```./appsettings.Development.json```. ```dotnet``` process should have read/write access to this folder. 
- Navigate to ```ProductCatalog/Web``` folder, using your favorite shell
- Run migrations:

```bash
dotnet ef database update -c ProductCatalogDbContext -p ProductCatalog.Web.csproj
```

## Run Api and Web Client

- Navigate to ```ProductCatalog/Web``` folder, using your favorite shell
- Restore client packages

```bash
npm install
```

- Run application:

```bash
dotnet run
```

- Open [http://localhost:5000](http://localhost:5000) in your favorite browser

## Api documentation

[http://localhost:5000/api/documentation/index.html](http://localhost:5000/api/documentation/index.html)

## Notes

### Api versioning

We have prefix ```/api/v1``` used. But, actually the verisioning is not implemented. If I think about reason of versioning then I see new requirements come in which are not compatible with the old one. But in the same time we do have to support old clients (e.g. mobile app). For that I would fork the existing git repository and implement new version of the api there. Then I would have different CI/CD cycles for the old api and new one.

### Integration testing

Due to luck of time I didnt make them. If I have a time I would go with docker compose to run the api, sql server. Iт this case it is relatively easy to restore the db state in between tests. Also is reduces dev ops involving into maintain the sql server, etc

### Unit tetsing

I would cover the ```DomainModel``` project with unit tests. The amount of test would be nearly equal to amount of requirements I've got (e.g. cover the code dupliocation, price confirmation). I didnt start with unit tests becuase if the ~8h estimate :) But I would like to write them. I am sure that having them would improve the domain model design.

### Photos uploading

I would change this probably to persist the files with extension. Then remove hardcoded content type from ```ProductCatalog.Api.Endpoints.Products.Get``` and use the file extention instead

### Domain exceptions handling

I would make it to send some data structure instead of plain error message, so the client can then uniquelly identify the error reason and handle it in correct way.

### Unhandled exceptions handling

I would catch them with separate middleware and log

### Swagger UI

I would run this as separate microservice

### Client side

Here I would add more better error handling, using maybe interseptions. Get rid of alerts. Add client validation, pagination. Use some some existing angular components to show list of products, etc...

### Code organization

- I would extract code under folder ```Web/Api/Extensions``` to separate project, to something like ```WebApi.Common```
- ```ProductCatalog.DomainModel.Services.IFilesStorage``` would be moved to separate project ```DomainModel.Services.Common```
- ```DomainException``` and all its generic children will go to ```DomainModel.Common```

### Prefferable development aproach

I would not go with DDD at first place. I would rather do quick implementation (put the logic inside controllers) to get fast feedback from product owner, etc. Then if everyone is satisfied with the functionality I would use the testing, DDD, optiomization to finilize the implemenation.

### Dev enviroment used to develop and test the app

 - OSX
 - Sql Server running in docker
 - VS Code
 - Google Chrome