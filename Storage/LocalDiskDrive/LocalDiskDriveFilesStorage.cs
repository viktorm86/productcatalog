﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ProductCatalog.DomainModel.Services;

namespace ProductCatalog.Storage.LocalDiskDrive
{
    public class LocalDiskDriveFilesStorage : IFilesStorage
    {
        private readonly string _rootFolder;
        private readonly ushort _bufferSizeInBytes;

        public LocalDiskDriveFilesStorage(IOptions<LocalDiskDriveOptions> options)
        {
            if (options == null || string.IsNullOrWhiteSpace(options.Value.FilesRootFolder))
            {
                throw new ArgumentNullException(nameof(options.Value.FilesRootFolder));
            }

            this._rootFolder = options.Value.FilesRootFolder;
            this._bufferSizeInBytes = options.Value.BufferSizeInBytes;
        }
        public async Task PersistAsync(string path, Stream file)
        {
            var pathToFile = Path.Combine(_rootFolder, path);
            var targetDirectory = Path.GetDirectoryName(pathToFile);
            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }
            using (var fileStream = File.OpenWrite(pathToFile))
            {
                var buffer = new byte[_bufferSizeInBytes];
                int readBytes;
                while ((readBytes = await file.ReadAsync(buffer, 0, _bufferSizeInBytes)) > 0)
                {
                    await fileStream.WriteAsync(buffer, 0, readBytes);
                }
            }
        }

        public Task<Stream> ReadAsync(string path)
        {
            return Task.FromResult(File.OpenRead(Path.Combine(_rootFolder, path)) as Stream);
        }
    }
}
