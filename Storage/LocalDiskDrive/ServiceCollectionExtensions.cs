using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.DomainModel.Services;

namespace ProductCatalog.Storage.LocalDiskDrive
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLocalDriveStorage(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<LocalDiskDriveOptions>(configuration.GetSection(nameof(LocalDiskDriveOptions)));
            services.AddScoped<IFilesStorage, LocalDiskDriveFilesStorage>();
            return services;
        }
    }
}