using System;

namespace ProductCatalog.Storage.LocalDiskDrive
{
    public class LocalDiskDriveOptions
    {
        public string FilesRootFolder { get; set; }
        public ushort BufferSizeInBytes { get; set; } = 1024;
    }
}