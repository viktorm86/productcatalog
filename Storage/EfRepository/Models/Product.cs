using System;

namespace ProductCatalog.Storage.EfRepository.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool PriceConfirmed { get; set; }
        public string Photo { get; set; }
        public DateTime LastUpdated { get; set; }
        public byte[] Timestamp { get; set; }
    }
}