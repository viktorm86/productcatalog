using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalog.Storage.EfRepository.Models;

namespace ProductCatalog.Storage.EfRepository.Models.Configs
{
    public class ProductTypeConfiguration : IEntityTypeConfiguration<Product>
    {
        public static readonly ProductTypeConfiguration Configuration = new ProductTypeConfiguration();

        private ProductTypeConfiguration() { }

        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.HasIndex(_ => _.Code).IsUnique();
            builder.Property(_ => _.Name).IsRequired();
            builder.Property(_ => _.Price).IsRequired();
            builder.Property(_ => _.Timestamp).IsRowVersion();
            builder.HasIndex(_ => _.Name);
        }
    }
}