using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.DomainModel;
using ProductCatalog.DomainModel.Common.Repositories;
using ProductCatalog.DomainModel.Repositories;

namespace ProductCatalog.Storage.EfRepository
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEfRepository(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProductCatalogDbContext>(options => options.UseSqlServer(configuration.GetValue<string>("ConnectionString")));
            services.AddScoped<IEntitiesSnapshotRepository<Product>, EfProductsShnapshotRepository>();
            services.AddScoped<IProductCatalogRepository, EfProductCatalogRepository>();
            return services;
        }
    }
}