﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.DomainModel;
using ProductCatalog.DomainModel.Common.Exceptions;
using ProductCatalog.DomainModel.Repositories;

namespace ProductCatalog.Storage.EfRepository
{
    public class EfProductCatalogRepository : IProductCatalogRepository
    {
        private readonly ProductCatalogDbContext _dbContext;

        public EfProductCatalogRepository(ProductCatalogDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task PersistAsync(DomainModel.ProductCatalog productCatalog)
        {
            foreach(var product in productCatalog)
            {
                var existingProduct = _dbContext.Products.Find(product.Id);
                if (product is DeletedProduct)
                {
                    if (existingProduct != null)
                    {
                        _dbContext.Products.Remove(existingProduct);
                    }
                }
                else
                {
                    if (existingProduct == null)
                    {
                        existingProduct = new Models.Product
                        {
                            Id = product.Id,
                            Code = product.Code
                        };
                        _dbContext.Add(existingProduct);
                    }
                    else
                    {
                        existingProduct.Timestamp = product.ConcurrencyToken;
                    }

                    existingProduct.Code = product.Code;
                    existingProduct.Name = product.Name;
                    existingProduct.Photo = product.Photo;
                    existingProduct.Price = product.Price;
                    existingProduct.LastUpdated = product.LastUpdated;
                    existingProduct.Timestamp = product.ConcurrencyToken;
                }
            }

            try 
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException exp) when(UniqueKeyViolation(exp))
            {
                throw new EntityDuplicationException<DomainModel.Product>(p => p.Code);
            }
        }

        public async Task<DomainModel.ProductCatalog> LoadWithProductAsync(Guid productId)
        {
            var dbProduct = await _dbContext.Products.FindAsync(productId);
            if (dbProduct != null)
            {
                return new DomainModel.ProductCatalog(
                    new DomainModel.Product(dbProduct.Id, dbProduct.Code, dbProduct.Name, 
                        dbProduct.Photo, dbProduct.Price, dbProduct.LastUpdated, dbProduct.Timestamp),
                    await LoadAllProductCodesAsync()
                );
            }
            else
            {
                return new DomainModel.ProductCatalog(await LoadAllProductCodesAsync());
            }
        }

        public async Task<DomainModel.ProductCatalog> LoadAsync()
        {
            return new DomainModel.ProductCatalog(await LoadAllProductCodesAsync());
        }

        private Task<ProductCode[]> LoadAllProductCodesAsync() =>_dbContext.Products
            .Select(_ => new ProductCode(_.Id, _.Code))
            .ToArrayAsync();


        private static bool UniqueKeyViolation(DbUpdateException exp)
            => exp.InnerException is SqlException sqlException 
                && (sqlException?.Number == 2601 || sqlException?.Number == 2627);
    }
}
