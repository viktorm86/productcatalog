using Microsoft.EntityFrameworkCore;
using ProductCatalog.Storage.EfRepository.Models;
using ProductCatalog.Storage.EfRepository.Models.Configs;

namespace ProductCatalog.Storage.EfRepository
{
    public class ProductCatalogDbContext: DbContext
    {
        public ProductCatalogDbContext(DbContextOptions<ProductCatalogDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.ApplyConfiguration(ProductTypeConfiguration.Configuration);
        }

        public DbSet<Product> Products { get; set; }
    }
}