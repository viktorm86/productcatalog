﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.DomainModel;
using ProductCatalog.DomainModel.Common.Repositories;

namespace ProductCatalog.Storage.EfRepository
{
    public class EfProductsShnapshotRepository : IEntitiesSnapshotRepository<Product>
    {
        private readonly ProductCatalogDbContext _dbContext;

        public EfProductsShnapshotRepository(ProductCatalogDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<EntitiesSnapshot<Product>> QueryAsync(int skip, int take)
        {
            var total = await _dbContext.Products.CountAsync();
            var products = await _dbContext.Products
                .OrderBy(p => p.Name)
                .Skip(skip)
                .Take(take)
                .ToArrayAsync();

            return new EntitiesSnapshot<Product>(
                products.Select(_ => new Product(_.Id, _.Code, _.Name, _.Photo, _.Price, _.LastUpdated, _.Timestamp)), 
                total
            );
        }
    }
}
