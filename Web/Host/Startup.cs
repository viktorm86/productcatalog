using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ProductCatalog.Web.Host
{
    public class Startup
    {
        private readonly Api.Startup _apiStartup;

        public Startup(IConfiguration configuration)
        {
            _apiStartup = new Api.Startup(configuration);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            _apiStartup.ConfigureServices(services);
            
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "./dist";
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            _apiStartup.Configure(app);

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "./Client";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
