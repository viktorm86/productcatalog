using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using ProductCatalog.Api.Endpoints.Common;

namespace ProductCatalog.Api.Extensions.Formatters
{
    public class CsvOutputFormatter : TextOutputFormatter
    {
        public CsvOutputFormatter()
        {
            SupportedMediaTypes.Clear();
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }
        protected override bool CanWriteType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(SnapshotModel<,>);
        }
        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var itemsPropInfo = context.Object.GetType().GetProperty("Items");
            var items = itemsPropInfo.GetValue(context.Object) as IEnumerable;
            foreach (var item in items)
            {
                var row = new List<string>();
                foreach (var itemProp in item.GetType().GetProperties())
                {
                    row.Add(itemProp.GetValue(item)?.ToString());
                }
                await context.HttpContext.Response.WriteAsync(string.Join(',', row));
                await context.HttpContext.Response.WriteAsync("\n");
            }
        }
    }
}