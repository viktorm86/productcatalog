using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Storage.EfRepository;
using ProductCatalog.Storage.LocalDiskDrive;

namespace ProductCatalog.Api.Extensions.Formatters
{
    internal static class ServiceCollectionExtensions
    {
        internal static IMvcBuilder AddCsvOutputFormatter(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.AddMvcOptions(options => 
                options.OutputFormatters.Add(new CsvOutputFormatter()));
        }
    }
}