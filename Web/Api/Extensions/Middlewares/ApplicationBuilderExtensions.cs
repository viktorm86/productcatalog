using Microsoft.AspNetCore.Builder;

namespace Web.Api.Extensions.Middlewares
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseDomainExceptionsHandling(this IApplicationBuilder app)
        {
            app.UseMiddleware<DomainExceptionsHanlerMiddleware>();
            return app;
        }
    }
}