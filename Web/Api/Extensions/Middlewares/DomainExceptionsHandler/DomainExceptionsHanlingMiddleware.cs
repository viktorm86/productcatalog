using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ProductCatalog.DomainModel;
using ProductCatalog.DomainModel.Common.Exceptions;

namespace Web.Api.Extensions.Middlewares
{
    public class DomainExceptionsHanlerMiddleware
    {
        private readonly RequestDelegate _next;
        public DomainExceptionsHanlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }
    
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(PriceConfirmationRequiredException exp)
            {
                await HandleExceptionAsync(httpContext, HttpStatusCode.UnprocessableEntity, exp.Message);
            }
            catch(EntityDuplicationException exp)
            {
                await HandleExceptionAsync(httpContext, HttpStatusCode.Conflict, exp.Message);
            }
            catch(EntityFieldRequiredException exp)
            {
                await HandleExceptionAsync(httpContext, HttpStatusCode.BadRequest, exp.Message);
            }
        }
    
        private static Task HandleExceptionAsync(HttpContext context, HttpStatusCode satusCode, string message)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)satusCode;
            return context.Response.WriteAsync(message);
        }
    }
}