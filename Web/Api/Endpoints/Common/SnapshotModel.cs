using System;
using System.Collections.Generic;
using System.Linq;
using ProductCatalog.DomainModel.Common.Repositories;

namespace ProductCatalog.Api.Endpoints.Common
{
    public class SnapshotModel<TDomainEntity, TItem>
    {
        public SnapshotModel(EntitiesSnapshot<TDomainEntity> entitiesSpanshot, Func<TDomainEntity, TItem> map) { 
            Items = entitiesSpanshot.Entities.Select(map);
            Total = entitiesSpanshot.Total;
        }

        public int Total { get; set; }
        public IEnumerable<TItem> Items { get; }
    }
}