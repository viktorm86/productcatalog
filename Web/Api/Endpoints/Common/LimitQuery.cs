using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace ProductCatalog.Api.Endpoints.Common
{
    public class LimitQuery
    {
        [FromQuery]
        public int Skip { get; set; } = 0;
        [FromQuery]
        public int Take { get; set; } = 50;
    }
}