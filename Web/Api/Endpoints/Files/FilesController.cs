using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.DomainModel.Services;

namespace ProductCatalog.Api.Endpoints.Products
{
    [Route("[controller]")]
    public class FilesController : Controller
    {
        private readonly IFilesStorage _filesStorage;

        public FilesController(IFilesStorage filesStorage)
        {
            _filesStorage = filesStorage;
        }

        [Route("{*path}")]
        [HttpGet]
        public async Task<IActionResult> Get(string path)
        {
            var fileStream = await _filesStorage.ReadAsync(path);
            return File(fileStream, "image/jpeg");
        }
    }
}
