using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.DomainModel;
using ProductCatalog.DomainModel.Common.Repositories;
using ProductCatalog.Api.Endpoints.Common;
using ProductCatalog.Api.Endpoints.Products.Models;
using ProductCatalog.DomainModel.Repositories;
using ProductCatalog.DomainModel.Common.Exceptions;
using ProductCatalog.DomainModel.Services;

namespace ProductCatalog.Api.Endpoints.Products
{
    [Route("[controller]")]
    public class ProductsController : Controller
    {
        private readonly IEntitiesSnapshotRepository<Product> _productsSnapshotRepository;
        private readonly IProductCatalogRepository _productCatalogRepository;
        private readonly IFilesStorage _filesStorage;

        public ProductsController( 
            IEntitiesSnapshotRepository<Product> productsSnapshotRepository,
            IProductCatalogRepository productCatalogRepository,
            IFilesStorage filesStorage)
        {
            _productsSnapshotRepository = productsSnapshotRepository;
            _productCatalogRepository = productCatalogRepository;
            _filesStorage = filesStorage;
        }

        [Route("")]
        [HttpGet]
        public async Task<IActionResult> Get(LimitQuery query)
        {
            var productsSnapshot = await _productsSnapshotRepository.QueryAsync(query.Skip, query.Take);
            return Ok(new SnapshotModel<Product, ProductModel>(productsSnapshot, ProductModel.MapFrom));
        }

        //TODO: Make errors catching generic using exception filter
        [Route("")]
        [HttpPost]
        [ProducesResponseType(typeof(PersistedProductModel), 201)]
        [ProducesResponseType(typeof(string), 409)]
        [ProducesResponseType(typeof(string), 422)]
        public async Task<IActionResult> Post([FromForm]AddOrUpdateProductRequest newProductRequest)
        {
            var product = new Product(newProductRequest.Code, newProductRequest.Name, newProductRequest.Price);
            var productCatalog = await _productCatalogRepository.LoadAsync();

            product = productCatalog.Add(product, newProductRequest.PriceConfirmed);

            if (newProductRequest.Photo != null)
            {
                product = product.SetPhoto();
                using(var photoStream = newProductRequest.Photo.OpenReadStream())
                {
                    await _filesStorage.PersistAsync(product.Photo, photoStream);
                }
                productCatalog.Update(product, newProductRequest.PriceConfirmed);
            }

            await _productCatalogRepository.PersistAsync(productCatalog);
            return Ok(new PersistedProductModel(product));
        }

        //TODO: Make errors catching generic using exception filter
        [Route("{id:guid}")]
        [HttpPut]
        [ProducesResponseType(typeof(PersistedProductModel), 201)]
        [ProducesResponseType(typeof(string), 409)]
        [ProducesResponseType(typeof(string), 422)]
        public async Task<IActionResult> Put(Guid id, [FromForm]AddOrUpdateProductRequest updateProductRequest)
        {
            var productCatalog = await _productCatalogRepository.LoadWithProductAsync(id);
            var product = productCatalog.GetProduct(id);

            if (product == null)
            {
                return NotFound();
            }

            product = product.Update(
                updateProductRequest.Code, 
                updateProductRequest.Name, 
                updateProductRequest.Price
            );

            if (updateProductRequest.Photo != null)
            {
                product = product.SetPhoto();
                using(var photoStream = updateProductRequest.Photo.OpenReadStream())
                {
                    await _filesStorage.PersistAsync(product.Photo, photoStream);
                }
            }
            
            productCatalog.Update(product, updateProductRequest.PriceConfirmed);
            await _productCatalogRepository.PersistAsync(productCatalog);
            return Ok(new PersistedProductModel(product));
        }

        [Route("{id:guid}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var productCatalog = await _productCatalogRepository.LoadWithProductAsync(id);
            var product = productCatalog.GetProduct(id);

            if (product == null)
            {
                return NotFound();
            }

            var deletedProduct = productCatalog.Delete(product);
            await _productCatalogRepository.PersistAsync(productCatalog);
            
            return Ok();
        }
    }
}
