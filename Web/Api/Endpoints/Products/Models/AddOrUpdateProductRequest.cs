using Microsoft.AspNetCore.Http;

namespace ProductCatalog.Api.Endpoints.Products.Models
{
    public class AddOrUpdateProductRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool PriceConfirmed { get; set; }
        public IFormFile Photo { get; set; }
    }
}