using System;
using ProductCatalog.DomainModel;

namespace ProductCatalog.Api.Endpoints.Products.Models
{
    public class PersistedProductModel
    {
        public PersistedProductModel(Product product)
        {
            Id = product.Id;
            LastUpdated = product.LastUpdated;
            Photo = product.Photo;
        }

        public Guid Id { get; }
        public DateTime LastUpdated { get; }
        public string Photo { get; set; }
    }
}