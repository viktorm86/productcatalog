using System;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.DomainModel;

namespace ProductCatalog.Api.Endpoints.Products.Models
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdated { get; set; }
        public byte[] ConcurrencyToken { get; set; }

        public static ProductModel MapFrom(Product product) {
            return new ProductModel {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name,
                Photo = product.Photo,
                Price = product.Price,
                LastUpdated = product.LastUpdated,
                ConcurrencyToken = product.ConcurrencyToken
            };
        }
    }
}