using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Api.Extensions.Formatters;
using ProductCatalog.Storage.EfRepository;
using ProductCatalog.Storage.LocalDiskDrive;
using Swashbuckle.AspNetCore.Swagger;
using Web.Api.Extensions.Middlewares;

namespace ProductCatalog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddCsvOutputFormatter();
            services.AddEfRepository(Configuration);
            services.AddLocalDriveStorage(Configuration);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Product Catalog", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.Map("/api/documentation", documentationBuilder => {
                documentationBuilder.UseSwagger();
                documentationBuilder.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/api/documentation/swagger/v1/swagger.json", "Product Catalog API V1");
                    c.RoutePrefix = "";
                });
            });
            app.Map("/api/v1", apiBuilder => {
                apiBuilder.UseDomainExceptionsHandling();
                apiBuilder.UseMvc();
            });
        }
    }
}
