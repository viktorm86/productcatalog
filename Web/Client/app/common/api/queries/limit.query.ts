export class LimitQuery {
  constructor(public skip: number, public take: number) { }
}