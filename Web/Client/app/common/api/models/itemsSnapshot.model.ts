export interface ItemsSnapshot<TItem> {
  total: number;
  items: TItem[];
}