import { IProduct } from "./services/models/product";

export class ProductViewModel implements IProduct {
  
  private _pendingChanges: ProductViewModel;
  private _priceConfirmationRequired: boolean;
  private _photoFile: File;
  
  id: string;
  code: string;
  name: string;
  photo: string;
  price: number;
  lastUpdated: Date;
  
  get photoFile(): File {
    return this._photoFile;
  }

  get photoUrl(): string {
    return `api/v1/files/${this.photo}`;
  }

  get hasPhoto(): boolean {
    return !!this.photo;
  }

  get new(): boolean {
    return this.id == null;
  }

  get view(): boolean {
    return !this.new && !this.edit;
  }

  get edit(): boolean {
    return this._pendingChanges != null;
  }

  get pendingChanges(): ProductViewModel {
    return this._pendingChanges;
  }

  get priceConfirmationRequired() {
    return this._priceConfirmationRequired;
  }

  setPhoto(event) {
    if(event.target.files.length > 0) {
      this._photoFile = event.target.files[0];
    }
  }

  requestPriceConfirmation() {
    this._priceConfirmationRequired = true;
  }

  startEdit() {
    return this._pendingChanges = this.clone();
  }

  rejectEdit() {
    this._pendingChanges = null;
    this._priceConfirmationRequired = false;
  }
  
  commitEdit() {
    this.code = this._pendingChanges.code;
    this.name = this._pendingChanges.name;
    this.price = this._pendingChanges.price;
    this._photoFile = this._pendingChanges.photoFile;
    this._pendingChanges = null;
    this._priceConfirmationRequired = false;
  }

  clone(): ProductViewModel {
    var productViewModel = new ProductViewModel();
    productViewModel.id = this.id;
    productViewModel.code = this.code;
    productViewModel.name = this.name;
    productViewModel.photo = this.photo;
    productViewModel._photoFile = this.photoFile;
    productViewModel.price = this.price;
    productViewModel.lastUpdated = this.lastUpdated;
    return productViewModel;
  }

  static fromProduct(product: IProduct): ProductViewModel {
    var productViewModel = new ProductViewModel();
    productViewModel.id = product.id;
    productViewModel.code = product.code;
    productViewModel.name = product.name;
    productViewModel.photo = product.photo
    productViewModel.price = product.price;
    productViewModel.lastUpdated = product.lastUpdated;
    return productViewModel;
  }
}