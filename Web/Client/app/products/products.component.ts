import { Component } from '@angular/core';
import { saveAs } from 'file-saver/FileSaver';
import { ProductViewModel } from './product.viewModel';
import { ProductsService } from './services/products.service';
import { LimitQuery } from '../common/api/queries/limit.query';
import { HttpErrorResponse } from '@angular/common/http';
import { INewProductRequest } from './services/models/newProduct.request';
import { UpdateProductRequest } from './services/models/updateProduct.request';

@Component({
  selector: 'products',
  templateUrl: './products.component.html'
})
export class ProductsComponent {
  
  products: ProductViewModel[];
  private _newProduct: ProductViewModel = new ProductViewModel();

  constructor(private _productsService: ProductsService) {
    _productsService.get(new LimitQuery(0, 50)).subscribe(result => {
      this.products = result.items.map(product => ProductViewModel.fromProduct(product));
      this._addNewProduct();
    });
  }

  create(product: ProductViewModel, confirmPrice: boolean = false) {
    let request: INewProductRequest = {
      code: product.pendingChanges.code,
      name: product.pendingChanges.name,
      price: product.pendingChanges.price,
      priceConfirmed: confirmPrice,
      photoFile: product.pendingChanges.photoFile
    };
    this._productsService.post(request).subscribe(persistedProduct => {
      product.id = persistedProduct.id;
      product.lastUpdated = persistedProduct.lastUpdated;
      product.photo = persistedProduct.photo;
      product.commitEdit();
      this._addNewProduct();
    }, 
    (err: HttpErrorResponse) => {
      alert(err.error);
      if (err.status == 422) {
        product.requestPriceConfirmation();
      }
    });
  }

  update(product: ProductViewModel, confirmPrice: boolean = false) {
    var request = new UpdateProductRequest(product.pendingChanges.id, product.pendingChanges.code, 
      product.pendingChanges.name, product.pendingChanges.price, confirmPrice, product.pendingChanges.photoFile);
    
    this._productsService.put(request).subscribe(persistedProduct => {
      product.lastUpdated = persistedProduct.lastUpdated;
      product.photo = persistedProduct.photo;
      product.commitEdit();
    }, 
    (err: HttpErrorResponse) => {
      alert(err.error);
      if (err.status == 422) {
        product.requestPriceConfirmation();
      }
    });
  }
  
  remove(product: ProductViewModel) {
    this._productsService.delete(product.id).subscribe(() => {
      var index = this.products.indexOf(product);
      this.products.splice(index, 1);
    });
  }

  exportToCsv() {
    this._productsService.getCsv(new LimitQuery(0, 50)).subscribe(response => {
      const blob = new Blob([response._body], { type: 'text/plain' });
      saveAs(blob, "products.csv");
    });
  }

  private _addNewProduct() {
    this._newProduct = new ProductViewModel();
    this._newProduct.startEdit();
    this.products.unshift(this._newProduct);
  }
}