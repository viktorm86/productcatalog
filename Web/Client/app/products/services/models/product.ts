export interface IProduct {
  id: string;
  code: string;
  name: string;
  photo: string;
  price: number;
  lastUpdated: Date;
}