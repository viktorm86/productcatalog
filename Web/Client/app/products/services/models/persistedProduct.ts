export interface IPersistedProduct {
  id: string;
  photo: string;
  lastUpdated: Date;
}