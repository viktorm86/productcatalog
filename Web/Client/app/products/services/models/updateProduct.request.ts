export class UpdateProductRequest {
  constructor(public id: string, public code: string, public name: string, public price: number, 
    public priceConfirmed: boolean, public photoFile: File) { }
}