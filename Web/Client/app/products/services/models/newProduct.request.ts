export interface INewProductRequest {
  code: string;
  name: string;
  price: number;
  priceConfirmed: boolean;
  photoFile: File;
}