import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LimitQuery } from '../../common/api/queries/limit.query';
import { ItemsSnapshot } from '../../common/api/models/itemsSnapshot.model';
import { IProduct } from './models/product';
import { INewProductRequest } from './models/newProduct.request';
import { IPersistedProduct } from './models/persistedProduct';
import { UpdateProductRequest } from './models/updateProduct.request';
import { Inject, Injectable } from '@angular/core';

@Injectable()
export class ProductsService {
  private _baseUrl = "api/v1/products";
  
  constructor(private _httpClient: HttpClient, private _http: Http, @Inject('BASE_URL') baseUrl: string) { }

  get(query: LimitQuery): Observable<ItemsSnapshot<IProduct>> {
    return this._httpClient.get<ItemsSnapshot<IProduct>>(
      this._baseUrl + `?skip=${query.skip}&take=${query.take}`
    );
  }

  getCsv(query: LimitQuery): Observable<any> {
    const headers = new Headers();
    headers.append('Accept', 'text/csv');
    return this._http.get(this._baseUrl + `?skip=${query.skip}&take=${query.take}`,
      { headers: headers }
    );
  }

  post(request: INewProductRequest): Observable<IPersistedProduct> {
    let requestBody = new FormData();
    if (request.code) {
      requestBody.append('code', request.code);
    }
    if (request.name) {
      requestBody.append('name', request.name);
    }
    if (request.price) {
      requestBody.append('price', request.price.toString());
    }
    requestBody.append('priceConfirmed', request.priceConfirmed.toString());
    if (request.photoFile) {
      requestBody.append('photo', request.photoFile);
    }
    return this._httpClient.post<IPersistedProduct>(this._baseUrl, requestBody);
  }

  put(request: UpdateProductRequest): Observable<IPersistedProduct> {
    let requestBody = new FormData();
    if (request.code) {
      requestBody.append('code', request.code);
    }
    if (request.name) {
      requestBody.append('name', request.name);
    }
    if (request.price) {
      requestBody.append('price', request.price.toString());
    }
    requestBody.append('priceConfirmed', request.priceConfirmed.toString());
    if (request.photoFile) {
      requestBody.append('photo', request.photoFile);
    }
    return this._httpClient.put<IPersistedProduct>(this._baseUrl + `/${request.id}`, requestBody);
  }

  delete(productId: string): Observable<any> {
    return this._httpClient.delete(this._baseUrl + `/${productId}`);
  }
}