using System;
using System.Threading.Tasks;

namespace ProductCatalog.DomainModel.Repositories
{
    public interface IProductCatalogRepository
    {
        Task PersistAsync(ProductCatalog productCatalog);
        Task<ProductCatalog> LoadWithProductAsync(Guid productId);
        Task<ProductCatalog> LoadAsync();
    }
}