using System;

namespace ProductCatalog.DomainModel
{
    public class DeletedProduct : Product
    {
        internal DeletedProduct(Product product)
            : base (product.Id, product.Code, product.Name, product.Photo, product.Price, 
                product.LastUpdated, product.ConcurrencyToken)
        {
        }
    }
}