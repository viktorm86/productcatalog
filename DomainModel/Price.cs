using System;

namespace ProductCatalog.DomainModel
{
    public struct Price
    {
        
        private decimal _price;

        public Price(decimal price)
        {
            if (price < 0) {
                throw new ArgumentException($"{nameof(price)} must be greater then 0");
            }

            _price = price;
        }

        public static implicit operator decimal(Price price) {
            return price._price;
        }

        public static implicit operator Price(decimal price) {
            return new Price(price);
        }
    }
}