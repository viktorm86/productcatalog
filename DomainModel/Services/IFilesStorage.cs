using System.IO;
using System.Threading.Tasks;

namespace ProductCatalog.DomainModel.Services
{
    public interface IFilesStorage
    {
        Task PersistAsync(string path, Stream file);
        Task<Stream> ReadAsync(string path);
    }
}