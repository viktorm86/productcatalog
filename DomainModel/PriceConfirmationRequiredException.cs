using System;
using System.Runtime.Serialization;
using ProductCatalog.DomainModel.Common.Exceptions;

namespace ProductCatalog.DomainModel
{
    [Serializable]
    public class PriceConfirmationRequiredException : DomainException
    {
        public PriceConfirmationRequiredException() { }

        public PriceConfirmationRequiredException(decimal maxPriceWithoutConfirmation)
            : base($"Price above {maxPriceWithoutConfirmation} requires confirmation") { }

        public PriceConfirmationRequiredException(string message) 
            : base(message) { }

        public PriceConfirmationRequiredException(string message, Exception innerException) 
            : base(message, innerException) { }

        protected PriceConfirmationRequiredException(SerializationInfo info, StreamingContext context) 
            : base(info, context) { }
    }
}