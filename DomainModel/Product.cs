using System;
using ProductCatalog.DomainModel.Common.Exceptions;

namespace ProductCatalog.DomainModel
{
    public class Product
    {
        public Product(string code, string name, Price price)
            : this(Guid.Empty, code, name, null, price, DateTime.UtcNow, null) {  }

        internal Product(Guid id, string code, string name, string photo, Price price, DateTime lastUpdated, byte[] concurrencyToken)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new EntityFieldRequiredException<Product>(_ => _.Code);
            }
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new EntityFieldRequiredException<Product>(_ => _.Name);
            }
            
            Id = id;
            Code = code;
            Name = name;
            Photo = photo;
            Price = price;
            LastUpdated = lastUpdated;
            ConcurrencyToken = concurrencyToken;
        }

        public Guid Id { get; }

        public string Code { get; }
            
        public string Name { get; }

        public string Photo { get; }

        public Price Price { get; }

        public DateTime LastUpdated { get; }

        public byte[] ConcurrencyToken { get; }

        public Product Update(string code, string name, Price price) =>
            new Product(Id, code, name, Photo, price, DateTime.UtcNow, ConcurrencyToken);
        

        public Product SetPhoto() =>
            new Product(Id, Code, Name, $"products/{Id}/photo", Price, DateTime.UtcNow, ConcurrencyToken);

        internal Product SetId(Guid id)
        {
            return new Product(id, Code, Name, Photo, Price, LastUpdated, ConcurrencyToken);
        }
    }
}