using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace ProductCatalog.DomainModel.Common.Exceptions
{
    [Serializable]
    public class EntityDuplicationException : DomainException
    {
        public EntityDuplicationException()
        {
        }

        public EntityDuplicationException(string message) : base(message)
        {
        }

        public EntityDuplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityDuplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class EntityDuplicationException<TEntity> : EntityDuplicationException
    {
        public EntityDuplicationException()
        {
        }

        public EntityDuplicationException(Expression<Func<TEntity, object>> prop) 
            : base($"Duplicated property: {((MemberExpression)prop.Body).Member.Name}")
        {
        }
        
        public EntityDuplicationException(string message) : base(message)
        {
        }

        public EntityDuplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityDuplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}