using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace ProductCatalog.DomainModel.Common.Exceptions
{
    [Serializable]
    public class EntityFieldRequiredException : DomainException
    {
        public EntityFieldRequiredException() { }

        public EntityFieldRequiredException(string message) : base(message) { }

        public EntityFieldRequiredException(string message, Exception innerException) 
            : base(message, innerException) { }

        protected EntityFieldRequiredException(SerializationInfo info, StreamingContext context) 
            : base(info, context) { }
    }

    [Serializable]
    public class EntityFieldRequiredException<TEntity> : EntityFieldRequiredException
    {
        public EntityFieldRequiredException() { }

        public EntityFieldRequiredException(Expression<Func<TEntity, object>> prop)
         : base($"Field {((MemberExpression)prop.Body).Member.Name} is required") { }

        public EntityFieldRequiredException(string message) : base(message) { }

        public EntityFieldRequiredException(string message, Exception innerException) 
            : base(message, innerException) { }

        protected EntityFieldRequiredException(SerializationInfo info, StreamingContext context) 
            : base(info, context) { }
    }
}