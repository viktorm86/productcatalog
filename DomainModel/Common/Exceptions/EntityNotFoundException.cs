using System;
using System.Runtime.Serialization;

namespace ProductCatalog.DomainModel.Common.Exceptions
{
    [Serializable]
    public class EntityNotFoundException<TEntity, TEntityId> : DomainException
    {
        public EntityNotFoundException() { }

        public EntityNotFoundException(Guid id)
         : base($"Can not find entity: {typeof(TEntity)} with id: {id}") { }

        public EntityNotFoundException(string message) : base(message) { }

        public EntityNotFoundException(string message, Exception innerException) 
            : base(message, innerException) { }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context) 
            : base(info, context) { }
    }
}