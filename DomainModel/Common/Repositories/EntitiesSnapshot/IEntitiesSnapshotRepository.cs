using System;
using System.Threading.Tasks;

namespace ProductCatalog.DomainModel.Common.Repositories
{
    public interface IEntitiesSnapshotRepository<TEntity>
    {
        Task<EntitiesSnapshot<TEntity>> QueryAsync(int skip, int take);
    }
}