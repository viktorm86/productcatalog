﻿using System.Collections.Generic;

namespace ProductCatalog.DomainModel.Common.Repositories
{
    public class EntitiesSnapshot<TEntity>
    {
        public EntitiesSnapshot(IEnumerable<TEntity> entities, int totalAmount) { 
            Entities = entities;
            Total = totalAmount;
        }

        public IEnumerable<TEntity> Entities { get; }
        public int Total { get; set; }
    }
}
