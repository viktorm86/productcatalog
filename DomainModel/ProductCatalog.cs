﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ProductCatalog.DomainModel.Common.Exceptions;

namespace ProductCatalog.DomainModel
{
    public class ProductCatalog: IEnumerable<Product>
    {
        private const decimal MaxPriceWithoutConfirmation = 999;
        private readonly List<Product> _products;
        private Dictionary<string, Guid> _takenProductCodes;

        internal ProductCatalog(Product product, IEnumerable<ProductCode> takenProductCodes)
            : this(takenProductCodes) {
            _products = new List<Product>() { product };
        }

        internal ProductCatalog(IEnumerable<ProductCode> takenProductCodes) {
            _products = new List<Product>();
            _takenProductCodes = takenProductCodes.ToDictionary(_ => _.Code, _ => _.ProductId);
        }

        public Product GetProduct(Guid id)
        {
            return _products.SingleOrDefault(_ => _.Id == id);
        }

        public Product Add(Product product, bool priceConfirmed)
        {   
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            if (_takenProductCodes.ContainsKey(product.Code)) {
                throw new EntityDuplicationException<Product>(_ => _.Code);
            }
            if (product.Price > MaxPriceWithoutConfirmation && !priceConfirmed)
            {
                throw new PriceConfirmationRequiredException(MaxPriceWithoutConfirmation);
            }
            product = product.SetId(Guid.NewGuid());
            _products.Add(product);
            return product;
        }

        public void Update(Product product, bool priceConfirmed)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            var originalProduct = _products.SingleOrDefault(_ => _.Id == product.Id);
            if (originalProduct == null) {
                throw new EntityNotFoundException<Product, Guid>(product.Id);
            }

            if (_takenProductCodes.TryGetValue(product.Code, out Guid productId) && product.Id != productId) {
                throw new EntityDuplicationException<Product>(_ => _.Code);
            }
            
            if (originalProduct.Price != product.Price && product.Price > MaxPriceWithoutConfirmation 
                && !priceConfirmed)
            {
                throw new PriceConfirmationRequiredException(MaxPriceWithoutConfirmation);
            }

            this._products.Remove(originalProduct);
            this._products.Add(product);
        }

        public Product Delete(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            product = _products.SingleOrDefault(_ => _.Id == product.Id);
            if (product == null) 
            {
                throw new EntityNotFoundException<Product, Guid>(product.Id);
            }

            this._products.Remove(product);
            var deleteProduct = new DeletedProduct(product);
            this._products.Add(deleteProduct);
            return deleteProduct;
        }

        public IEnumerator<Product> GetEnumerator() => this._products.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
