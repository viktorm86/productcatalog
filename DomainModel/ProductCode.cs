using System;

namespace ProductCatalog.DomainModel
{
    internal class ProductCode
    {
        internal ProductCode(Guid productId, string code)
        {
            this.ProductId = productId;
            this.Code = code;

        }
        public Guid ProductId { get; }
        public string Code { get; }
    }
}